package com.teamone.sedes.Services;

import com.teamone.sedes.Model.UserModel;
import com.teamone.sedes.Respositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository ;

    public List<UserModel> findAll() {
        System.out.println("findAll UserService");

        return this.userRepository.findAll();
    }

    public Optional<UserModel> findById(String id){
        System.out.println("En findByID de UserService");

        return  this.userRepository.findById(id);
    }

    public List<UserModel> findByOrderBySedeAsc() {
        System.out.println("Orderby UserService");

        return this.userRepository.findByOrderBySedeAsc();
    }
    public boolean add(UserModel user){
        System.out.println("add UserService");
        boolean result = true;
        if (this.findById(user.getId()).isPresent() == true){
            System.out.println("Usuario ya registrado");
            result = true;
        }
        if (this.findById(user.getId()).isPresent() == false){
            System.out.println("Usuario ha registrar");
            this.userRepository.save(user);
            result = false;
        }

        return result;
    }

    public UserModel update(UserModel user){
        System.out.println("Update en UserService");

        return this.userRepository.save(user);
    }

    public boolean delete(String id){
        System.out.println("delete desde userService");
        boolean result = false;

        if (this.findById(id).isPresent() == true){
            System.out.println("Usuario encontrado borrado");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }


}


