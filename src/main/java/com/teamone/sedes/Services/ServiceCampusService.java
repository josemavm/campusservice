package com.teamone.sedes.Services;

import com.teamone.sedes.Model.ServiceCampusModel;
import com.teamone.sedes.Respositories.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import java.util.Optional;

@Service
public class ServiceCampusService {

    @Autowired
    ServiceRepository serviceRepository;



    public Optional<ServiceCampusModel> findById(String id){
        System.out.println("findById");
        return this.serviceRepository.findById(id);

    }
    public Optional<ServiceCampusModel> findByIdSede2(String sede){
        System.out.println("findById");
        return this.serviceRepository.findBySede(sede);

    }

    public boolean delete(String id){

        boolean result= false;

        System.out.println("delete en productService");

        if(this.findById(id).isPresent() == true) {
            System.out.println("Producto a eliminar encontrado");
            this.serviceRepository.deleteById(id);
            result=true;
        }
        return result;

    }
    public ServiceCampusModel update( ServiceCampusModel serviceCampusModel) {
        System.out.println("update service");

        return this.serviceRepository.save(serviceCampusModel);
    }

    public List<ServiceCampusModel> findAll() {
        System.out.println("findAll UserService");

        return this.serviceRepository.findAll();
    }

    public boolean add(ServiceCampusModel services)
    {
        System.out.println("add servicio");

        boolean result = true;

        if ( this.findById(services.getId()).isPresent() == true )
        {
            System.out.println("El servicio  " + services.getName() +  "  ya existe " );

            result = true;
        }
        else
        {
            System.out.println("Se ha registrado el servicio " +services.getName());

            this.serviceRepository.save(services);
            result = false;
        }
        return result;
    }

}
