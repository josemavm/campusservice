package com.teamone.sedes.Controller;

import com.teamone.sedes.Model.ServiceCampusModel;
import com.teamone.sedes.Services.ServiceCampusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/campusservice/v1")

public class ServicesCampusController {
    @Autowired
    ServiceCampusService campusService;

    @GetMapping("/services")
    public ResponseEntity<List<ServiceCampusModel>> getServices(){
        System.out.println("getServices");

        return new ResponseEntity<>(
                this.campusService.findAll()
                , HttpStatus.OK
        );
    }

    @GetMapping("/services/{id}")
    public ResponseEntity<Object>getServiceById(@PathVariable String id) {
        System.out.println("getServiceById");
        System.out.println("El ID del servicio a buscar es: " + id);

        Optional<ServiceCampusModel> result = this.campusService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "ID de servicio no registrado"
                , result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @GetMapping("/services/sede/{sede}")
    public ResponseEntity<Object> getServiceBySede(@PathVariable String sede){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es " + sede);

        Optional<ServiceCampusModel> result = this.campusService.findByIdSede2(sede);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Servicios no encontrados",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PostMapping("/services")
    public ResponseEntity<ServiceCampusModel> addService (@RequestBody ServiceCampusModel services) {

        System.out.println("addService");
        System.out.println("Id del servicio a crear " + services.getId());
        System.out.println("Nombre del servicio a crear " + services.getName());
        System.out.println("descripcion del servicio a crear " + services.getDescription());

        return new ResponseEntity (
                this.campusService.add(services)
                , HttpStatus.CREATED
        );
    }

    @PutMapping("/services/{id}")
    public ResponseEntity<ServiceCampusModel> updateservice(@RequestBody ServiceCampusModel services, @PathVariable String id) {
        System.out.println("updateservice");
        System.out.println("El id del servicio a actualizar en parametro URL es " + id);
        System.out.println("El id del servicio a actualizar es " + services.getId());
        System.out.println("El nombre del servicio a actualizar es " + services.getName());

        Optional<ServiceCampusModel> serviceToUpdate = this.campusService.findById(id);

        if (serviceToUpdate.isPresent()) {
            System.out.println("El Servicio disponible, actualizando");
            this.campusService.update(services);
        }

        return new ResponseEntity<>(
                services,
                serviceToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/services/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {

        System.out.println("deleteUser");

        boolean deleteUser = this.campusService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Servicio en sede borrado" : "Servicio en sede no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


}
