package com.teamone.sedes.Controller;

import com.teamone.sedes.Model.UserModel;
import com.teamone.sedes.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/campusservice/v1")
public class UserController {


    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(){
        System.out.println("getUser");

        return  new ResponseEntity<>(
                this.userService.findAll()
                , HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUsersById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("El id a buscar es: " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "id de usuario no registrado"
                ,result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @GetMapping("/users/sede")
    public ResponseEntity<List<UserModel>> getUsersSede(){
        System.out.println("getUsersSede");

        return  new ResponseEntity<>(
                this.userService.findByOrderBySedeAsc()
                , HttpStatus.OK
        );
    }


    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del usuario que se va crear es: " + user.getId());
        System.out.println("El nombre del usuario que se va crear es: " + user.getName());
        System.out.println("La sede del usuario que se va crear es: " + user.getSede());

        boolean addUser = this.userService.add(user);

        return new ResponseEntity(
                addUser ? "ID existente" : " usuario creado",
                addUser ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> UpdateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("UpdateUser");
        System.out.println("El id a buscar es: " + id);
        System.out.println("La id del user que se actualiza es: " + user.getId());
        System.out.println("El nombre del user que se actualiza es: " + user.getName());
        System.out.println("La sede del user que se actualiza es: " + user.getSede());

        Optional<UserModel> UserUpdate = this.userService.findById(id);

        if(UserUpdate.isPresent()){
            System.out.println("Usuario para actualizar encontrado , actualizando");
            this.userService.update(user);
        }

        return new ResponseEntity<>(
                user
                ,UserUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");

        boolean deletedUser = this.userService.delete(id);


        return new ResponseEntity<>(
                deletedUser ? "usuario borrado" : "usuario no borrado",
                deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );
    }

}
