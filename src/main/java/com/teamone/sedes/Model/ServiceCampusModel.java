package com.teamone.sedes.Model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.stream.Stream;

@Document(collection = "servicescampus")
public class ServiceCampusModel {
    private String id;
    private String name;
    private String description;
    private String horario;
    private String sede;
    private boolean torre;
    private boolean polanco;


    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public ServiceCampusModel() {
    }

    public ServiceCampusModel(String id, String name, String description, String horario, String sede, boolean torre, boolean polanco) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.horario = horario;
        this.sede = sede;
        this.torre = torre;
        this.polanco = polanco;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public boolean isTorre() {
        return torre;
    }

    public void setTorre(boolean torre) {
        this.torre = torre;
    }

    public boolean isPolanco() {
        return polanco;
    }

    public void setPolanco(boolean polanco) {
        this.polanco = polanco;
    }
}
