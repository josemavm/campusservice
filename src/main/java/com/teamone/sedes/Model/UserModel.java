package com.teamone.sedes.Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class UserModel {

    @Id
    private String id;
    private String name;
    private String sede;

    public UserModel(){

    }

    public UserModel(String id, String name, String sede) {
        this.id = id;
        this.name = name;
        this.sede = sede;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSede() {

        return this.sede;
    }

    public void setSede(String sede) {

        this.sede = sede;
    }


}
