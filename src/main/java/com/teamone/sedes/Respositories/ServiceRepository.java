package com.teamone.sedes.Respositories;

        import com.teamone.sedes.Model.ServiceCampusModel;
        import org.springframework.data.mongodb.repository.MongoRepository;
        import org.springframework.stereotype.Repository;
        import java.util.Optional;

@Repository

public interface ServiceRepository extends MongoRepository<ServiceCampusModel,String>{
        Optional<ServiceCampusModel> findBySede(String sede);
}
